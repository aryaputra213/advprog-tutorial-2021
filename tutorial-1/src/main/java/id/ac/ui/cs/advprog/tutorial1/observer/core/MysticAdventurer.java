package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    @Override
    public void update() {
        //ToDo: Complete Me
        if (this.guild.getQuestType().equals("E") || this.guild.getQuestType().equals("D")) {
            getQuests().add(guild.getQuest());
        }
    }


}

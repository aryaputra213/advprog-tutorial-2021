package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public abstract class Adventurer {

    private AttackBehavior attackBehavior;
    private DefenseBehavior defenseBehavior;

    // tambahan constructor karena tipe data attack dan defense private
    public  Adventurer(AttackBehavior attackBehavior, DefenseBehavior defenseBehavior) {
        this.attackBehavior = attackBehavior;
        this.defenseBehavior = defenseBehavior;
    }
    public String attack () {
        return attackBehavior.attack();
    }

    public String defend() {
        return defenseBehavior.defend();
    }

    public Adventurer() {}

    public abstract String getAlias();

    public void setAttackBehavior(AttackBehavior attackBehavior) {
        this.attackBehavior = attackBehavior;
    }

    public AttackBehavior getAttackBehavior() {

        return attackBehavior;
    }

    public void setDefenseBehavior(DefenseBehavior defenseBehavior) {

        this.defenseBehavior = defenseBehavior;
    }

    public DefenseBehavior getDefenseBehavior() {

        return defenseBehavior;
    }
}

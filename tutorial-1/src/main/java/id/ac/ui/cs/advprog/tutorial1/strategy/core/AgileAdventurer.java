package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
	
    //ToDo: Complete me
    public AgileAdventurer() {
        super(new AttackWithGun(),new DefendWithBarrier());
    }

    //To Do (Arya)
    @Override
    public String getAlias() {
        return "Agile";
    }
}

package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
// New import
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aimMode;
    
    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.aimMode = false;
    }
    @Override
    public String normalAttack() {
        return bow.shootArrow(this.aimMode);
    }

    @Override
    public String chargedAttack() {
        this.aimMode = !this.aimMode;
        if (aimMode) {
            return "Entered aim shot mode";
        } else {
            return "Leaving aim shot mode";
        }
    }
    
    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}

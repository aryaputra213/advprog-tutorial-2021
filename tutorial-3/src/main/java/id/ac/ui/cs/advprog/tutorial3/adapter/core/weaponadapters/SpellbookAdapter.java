package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// New Import
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {
    
    private Spellbook spellbook;
    private boolean ultimate;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.ultimate = false;
    }

    @Override
    public String normalAttack() {
        this.ultimate = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (this.ultimate) {
            this.ultimate = false;
            return spellbook.smallSpell();
        } else {
            this.ultimate = true;
            return spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}

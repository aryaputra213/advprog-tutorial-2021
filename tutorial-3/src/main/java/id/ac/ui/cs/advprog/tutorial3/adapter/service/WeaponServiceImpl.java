package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.validation.constraints.Null;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)
    // private final WeaponRepository weaponRepository;
    // private final SpellbookRepository spellbookRepository;
    // private final BowRepository bowRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;
    
    @Autowired
    private SpellbookRepository spellbookRepository;
    
    @Autowired
    private BowRepository bowRepository;

    // Add cunstructor (ARYA)
    public WeaponServiceImpl() {
    }
    
    // TODO: implement me 
    @Override
    public List<Weapon> findAll() {
        for (int i = 0 ; i < spellbookRepository.findAll().size() ; i++) {
            if (weaponRepository.findByAlias(spellbookRepository.findAll().get(i).getName()) == null) {
                SpellbookAdapter spellbookAdapter = new SpellbookAdapter(spellbookRepository.findAll().get(i));
                weaponRepository.save(spellbookAdapter);
            }
        }
        for (int i = 0 ; i < bowRepository.findAll().size() ; i++) {
            if (weaponRepository.findByAlias(bowRepository.findAll().get(i).getName()) == null) {
                 BowAdapter bowAdapter = new BowAdapter(bowRepository.findAll().get(i));
                weaponRepository.save(bowAdapter);
            }
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon senjata = weaponRepository.findByAlias(weaponName);
        if (attackType == 1) {
            logRepository.addLog(senjata.getHolderName() + " attacked with " + senjata.getName() + "(normal attack): " +  senjata.normalAttack());
        } else {
            logRepository.addLog(senjata.getHolderName() + " attacked with " + senjata.getName() + "(charged attack): " +  senjata.chargedAttack());
        }
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}

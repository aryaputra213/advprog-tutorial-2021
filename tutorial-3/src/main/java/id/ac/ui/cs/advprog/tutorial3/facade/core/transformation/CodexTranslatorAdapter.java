package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;


import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CodexTranslatorAdapter implements Kripto {
    public Spell encode(Spell spell) {
        return CodexTranslator.translate(spell, RunicCodex.getInstance());
    }

    public Spell decode(Spell spell) {
        return CodexTranslator.translate(spell, AlphaCodex.getInstance());
    }
}

package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class ExecutionClass {
    private static Kripto[] TRANSLATOR = {new CelestialTransformation(), new AbyssalTransformation(), new FoundingTitanTransformation(), new CodexTranslatorAdapter()};
    

    public static Spell encode(Spell spell) {
        for (int i = 0 ; i < TRANSLATOR.length ; i++) {
            spell = TRANSLATOR[i].encode(spell);
        }
        return spell;
    }

    public static Spell decode(Spell spell) {
        for (int i = TRANSLATOR.length-1 ; i >= 0 ; i--) {
            spell = TRANSLATOR[i].decode(spell);
        }
        return spell;

    }
}

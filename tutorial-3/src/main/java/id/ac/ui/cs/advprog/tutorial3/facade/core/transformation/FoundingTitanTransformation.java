package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.Arrays;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;


public class FoundingTitanTransformation implements Kripto {

    public FoundingTitanTransformation() {}

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        char[] res = new char[text.length()];
        if (selector == 1) {
            int a = 0;
            for (int i = res.length-1 ; i >= 0 ; i--) {
                res[a] = text.charAt(i);
                a++;
            }
        } else {
            int a = text.length()-1;
            for (int i = 0 ; i < text.length() ; i++) {
                res[i] = text.charAt(a);
                a--;
            }
        }
        return new Spell(new String(res), codex);
    }
    
}

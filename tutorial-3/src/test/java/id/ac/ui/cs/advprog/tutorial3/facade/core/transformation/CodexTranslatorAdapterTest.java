package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CodexTranslatorAdapterTest {
    private Class<?> codexAdaptorClass;

    @BeforeEach
    public void setup() throws Exception {
        codexAdaptorClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CodexTranslatorAdapter");
    }

    @Test
    public void testcTranslatorHasEncodeMethod() throws Exception {
        Method translate = codexAdaptorClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testcTranslatorHasDecodeMethod() throws Exception {
        Method translate = codexAdaptorClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testcTranslatorEncodesCorrectly() throws Exception {
        CodexTranslatorAdapter codexTranslatorAdapter = new CodexTranslatorAdapter();
        String text = "kata kakeku kuku kaki kakaku kaku kaku";
        Spell spellDummy = codexTranslatorAdapter.encode(new Spell(text, AlphaCodex.getInstance()));
        String expected = "aJMJ_aJaxaA_aAaA_aJan_aJaJaA_aJaA_aJaA";
        assertEquals(expected, spellDummy.getText());
    }

    @Test
    public void testcTranslatorDecodesCorrectly() throws Exception {
        CodexTranslatorAdapter codexTranslatorAdapter = new CodexTranslatorAdapter();
        String text = "aJMJ_aJaxaA_aAaA_aJan_aJaJaA_aJaA_aJaA";
        Spell spellDummy = codexTranslatorAdapter.decode(new Spell(text, RunicCodex.getInstance()));
        String expected = "kata kakeku kuku kaki kakaku kaku kaku";
        assertEquals(expected, spellDummy.getText());
    }
}

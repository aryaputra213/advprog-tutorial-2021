package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;


import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExecutionClassTest {
    private Class<?> execuClass;

    @BeforeEach
    public void setup() throws Exception {
        execuClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.ExecutionClass");
    }

    @Test
    public void testcTranslatorHasEncodeMethod() throws Exception {
        Method encode = execuClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testcTranslatorHasDecodeMethod() throws Exception {
        Method decode = execuClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testcTranslatorEncodesCorrectly() throws Exception {
        Spell spell = new Spell("this is dummy test", AlphaCodex.getInstance());
        Spell spellDummy = ExecutionClass.encode(spell);
        String result = "_B)GV=Md;{Ab!_^){i";
        assertEquals(result, spellDummy.getText());
    }

    @Test
    public void testcTranslatorDecodesCorrectly() throws Exception {
        Spell spell = new Spell( "_B)GV=Md;{Ab!_^){i", RunicCodex.getInstance());
        Spell spellDummy = ExecutionClass.decode(spell);
        String result = "this is dummy test";
        assertEquals(result, spellDummy.getText());
    }
}

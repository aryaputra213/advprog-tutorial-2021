package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FoundingTitanTransformationTest {
    private Class<?> foundingClass;

    @BeforeEach
    public void setup() throws Exception {
        foundingClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FoundingTitanTransformation");
    }

    @Test
    public void testFoundingHasEncodeMethod() throws Exception {
        Method translate = foundingClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFoundingHasDecodeMethod() throws Exception {
        Method translate = foundingClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFoundingEncodesCorrectly() throws Exception {
        String text = "kata kakeku kuku kaki kakaku kaku kaku";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "ukak ukak ukakak ikak ukuk ukekak atak";

        Spell result = new FoundingTitanTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test 
    public void testFoundingDecocesCorrectly() throws Exception {
        String text = "ukak ukak ukakak ikak ukuk ukekak atak";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "kata kakeku kuku kaki kakaku kaku kaku";

        Spell result = new FoundingTitanTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }
}

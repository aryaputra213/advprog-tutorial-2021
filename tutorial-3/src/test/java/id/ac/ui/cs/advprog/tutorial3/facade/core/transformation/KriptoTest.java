package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KriptoTest {
    private Class<?> kriptoClass;

    @BeforeEach
    public void setup() throws Exception {
        kriptoClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Kripto");
    }

    @Test
    public void testKriptoIsAPublicInterface() {
        int classModifiers = kriptoClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testBowHasencodeAbstractMethod() throws Exception {
        Class<?>[] encodeArgs = new Class[1];
        encodeArgs[0] = Spell.class;
        Method encode = kriptoClass.getDeclaredMethod("encode", encodeArgs);
        int methodModifiers = encode.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, encode.getParameterCount());
    }

    @Test
    public void testBowHasdecodeAbstractMethod() throws Exception {
        Class<?>[] decodeArgs = new Class[1];
        decodeArgs[0] = Spell.class;
        Method decode = kriptoClass.getDeclaredMethod("decode", decodeArgs);
        int methodModifiers = decode.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, decode.getParameterCount());
    }
}

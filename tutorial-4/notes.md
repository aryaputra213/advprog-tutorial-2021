# Notes

## Lazy Instantiation

Keuntungan:

- Menghemat penggunaan memori.
- Run-time awal bisa lebih lancar.
- Instansiasi saat memang dibutuhkan.

Kerugian:

- Terjadi kesulitan dalam kasus multithreading, yaitu pemanggilan getInstance() secara bersamaan.

Apabila pemanggilan getInstance() secara terus-menerus, maka akan ada pengecekan apakah objek sudah ada atau belum berkali-kali.

## Eager Instantiation

Keuntungan:

- Multithreading bisa ditangani dengan mudah, karena mengembalikan objek yang sama dari awal objek tersebut dibuat.
- Pemanggilan getInstance() berkali-kali sangat cocok.

Kekurangan:

- Instansiasinya akan percuma dan membuang memori jika objek ternyata tidak diperlukan
- Banyak resource yang langsung digunakan (Walaupun jarang digunakan) 
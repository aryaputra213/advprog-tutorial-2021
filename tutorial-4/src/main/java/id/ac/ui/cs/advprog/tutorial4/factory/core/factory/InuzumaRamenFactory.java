package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class InuzumaRamenFactory implements MenuFactory{
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    @Override
    public Flavor createFlavor() {
        // TODO Auto-generated method stub
        return new Spicy();
    }

    @Override
    public Meat createMeat() {
        // TODO Auto-generated method stub
        return new Pork();
    }

    @Override
    public Noodle createNoodle() {
        // TODO Auto-generated method stub
        return new Ramen();
    }

    @Override
    public Topping createTopping() {
        // TODO Auto-generated method stub
        return new BoiledEgg();
    }
    
}

package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class SnevnezhaShiratakiFactory implements MenuFactory {
    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami

    @Override
    public Flavor createFlavor() {
        // TODO Auto-generated method stub
        return new Umami();
    }

    @Override
    public Meat createMeat() {
        // TODO Auto-generated method stub
        return new Fish();
    }

    @Override
    public Noodle createNoodle() {
        // TODO Auto-generated method stub
        return new Shirataki();
    }

    @Override
    public Topping createTopping() {
        // TODO Auto-generated method stub
        return new Flower();
    }
    
}

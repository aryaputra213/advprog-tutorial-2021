package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuFactoryTest {
    private Class<?> menuFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        menuFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory");
    }

    @Test
    public void testMenuFactoryIsAPublicInterface() {
        int classModifiers = menuFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMenuFactoryHasCreateFlavorAbstractMethod() throws Exception {
        Method createFlavor = menuFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createFlavor.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasCreateMeatAbstractMethod() throws Exception {
        Method createMeat = menuFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createMeat.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasCreateNoodleAbstractMethod() throws Exception {
        Method createNoodle = menuFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createNoodle.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasCreateToppingAbstractMethod() throws Exception {
        Method createTopping = menuFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createTopping.getParameterCount());
    }
}

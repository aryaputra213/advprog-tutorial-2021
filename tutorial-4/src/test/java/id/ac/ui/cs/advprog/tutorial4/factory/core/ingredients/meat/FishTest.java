package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FishTest {
    private Class<?> fishTest;

    @BeforeEach
    public void setUp() throws Exception {
        fishTest = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish");
    }

    @Test
    public void testfishTestIsPublicClass() {
        assertTrue(Modifier.isPublic(fishTest.getModifiers()));
    }

    @Test
    public void testfishTestIsAMeat() {
        Collection<Type> interfaces = Arrays.asList(fishTest.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testfishTestOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = fishTest.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testIonicBowGetDescriptionMethodReturns() throws Exception {
        Fish fish = new Fish();
        assertEquals("Adding Zhangyun Salmon Fish Meat...",fish.getDescription());
    }
}

package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuTest {

    private Class<?> menuClass;


    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsConcreteClass() {
        assertFalse(Modifier.isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testMenuHasGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test 
    public void testMenuHasgetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void testMenuHasgetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void testMenuHasgetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
        assertEquals(0, getFlavor.getParameterCount());
    }

    @Test

    public void testOutputOfAllMethodForInuzumaRamen() throws Exception {
        Menu inuzuma = new InuzumaRamen("dummy menu");
        assertEquals("dummy menu", inuzuma.getName());
        assertEquals("Adding Guahuan Boiled Egg Topping", inuzuma.getTopping().getDescription());
        assertEquals("Adding Inuzuma Ramen Noodles...", inuzuma.getNoodle().getDescription());
        assertEquals("Adding Tian Xu Pork Meat...", inuzuma.getMeat().getDescription());
        assertEquals("Adding Liyuan Chili Powder...", inuzuma.getFlavor().getDescription());
    }

    @Test

    public void testOutputOfAllMethodForMondoUdon() throws Exception {
        Menu mondoUdon = new MondoUdon("dummy menu");
        assertEquals("dummy menu", mondoUdon.getName());
        assertEquals("Adding Shredded Cheese Topping...", mondoUdon.getTopping().getDescription());
        assertEquals("Adding Mondo Udon Noodles...", mondoUdon.getNoodle().getDescription());
        assertEquals("Adding Wintervale Chicken Meat...", mondoUdon.getMeat().getDescription());
        assertEquals("Adding a pinch of salt...", mondoUdon.getFlavor().getDescription());
    }

    @Test

    public void testOutputOfAllMethodForSnevnezhaShirataki() throws Exception {
        Menu shirataki = new SnevnezhaShirataki("dummy menu");
        assertEquals("dummy menu", shirataki.getName());
        assertEquals("Adding Xinqin Flower Topping...", shirataki.getTopping().getDescription());
        assertEquals("Adding Snevnezha Shirataki Noodles...", shirataki.getNoodle().getDescription());
        assertEquals("Adding Zhangyun Salmon Fish Meat...", shirataki.getMeat().getDescription());
        assertEquals("Adding WanPlus Specialty MSG flavoring...", shirataki.getFlavor().getDescription());
    }

    @Test

    public void testOutputOfAllMethodForLiyuanSoba() throws Exception {
        Menu liyuan = new LiyuanSoba("dummy menu");
        assertEquals("dummy menu", liyuan.getName());
        assertEquals("Adding Shiitake Mushroom Topping...", liyuan.getTopping().getDescription());
        assertEquals("Adding Liyuan Soba Noodles...", liyuan.getNoodle().getDescription());
        assertEquals("Adding Maro Beef Meat...", liyuan.getMeat().getDescription());
        assertEquals("Adding a dash of Sweet Soy Sauce...", liyuan.getFlavor().getDescription());
    }
}


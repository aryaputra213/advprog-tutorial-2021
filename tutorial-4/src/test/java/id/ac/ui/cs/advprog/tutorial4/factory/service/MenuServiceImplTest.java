package id.ac.ui.cs.advprog.tutorial4.factory.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MenuServiceImplTest {

    private MenuServiceImpl menuServiceImpl;

    @BeforeEach
    public void setUp() throws Exception {
        menuServiceImpl = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceImplHasGetMenusImplementation() {
        assertEquals(4, menuServiceImpl.getMenus().size());
    }

    @Test
    public void testMenuServiceImplHasCreateMenuImplementation() {
        menuServiceImpl.createMenu("Dummy Indomie", "Soba");
        menuServiceImpl.createMenu("Dummy Popmie", "Udon");

        assertEquals(6, menuServiceImpl.getMenus().size());
    }
}

package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderDrinkTest {

    @Test
    public void testOrderDrinkOnlyCreatedOnce() {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();

        assertThat(orderDrink1).isEqualToComparingFieldByField(orderDrink2);
    }

}


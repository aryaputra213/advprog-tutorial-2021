package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderFoodTest {

    @Test
    public void testOrderDrinkOnlyCreatedOnce() {
        OrderFood orderFood = OrderFood.getInstance();
        OrderFood orderFood2 = OrderFood.getInstance();

        assertThat(orderFood).isEqualToComparingFieldByField(orderFood2);
    }

}

package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class OrderServiceImplTest {
    private Class<?> orderServiceImpl;

    @Mock
    OrderDrink orderDrink;

    @Mock
    OrderFood orderFood;

    @Spy
    OrderServiceImpl orderService = new OrderServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        orderServiceImpl = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService");
        orderDrink = mock(OrderDrink.class);
        orderFood = mock(OrderFood.class);
    }
    @Test
    public void testOrderServiceOrderAFoodImplementedCorrectly() throws Exception {
         String food = "Fried Dummy";
         doNothing().when(orderFood).setFood(isA(String.class));
         when(orderFood.getFood()).thenReturn(food);

        orderService.orderAFood("Dummy Food");
        assertEquals("Dummy Food", orderService.getFood().getFood());
    }

    @Test
    public void testOrderServiceOrderADrinkImplementedCorrectly() throws Exception {
         String drink = "Dummy Juice";
         doNothing().when(orderDrink).setDrink(isA(String.class));
         when(orderDrink.getDrink()).thenReturn(drink);

        orderService.orderADrink("Dummy Drink");
        assertEquals("Dummy Drink", orderService.getDrink().getDrink());
    }

}


## Recruiements

- [✅] Mahasiswa dapat mendaftar ke satu mata kuliah (langsung diterima)
- [✅] Mata kuliah yang sudah ada asisten tidak bisa dihapus
- [✅] Mahasiswa yang sudah menjadi asisten di suatu mata kuliah tidak bisa dihapus juga.
- [✅] Mahasiswa dapat membuat, mengupdate, dan menghapus log dan sistem akan menyimpannya.
- [✅] Setiap mahasiswa bekerja akan dicatat pada sebuah log.
- [✅] Menyiapkan API untuk memperlihatkan laporan pembayaran
- [✅] Laporan pembayaran, mahasiswa dapat melihat laporan untuk bulan tertentu.
- [✅] Pada laporan terdapat data bulan, jam kerja (jam), dan pembayaran (Greil)
- [✅] Sebuah log dapat saja berisikan data lebih singkat dari satu jam.




## Catetan pribadi

- Membuka lowongan mata kuliah yang membutuhkan asisten
- Mahasiswa dapat melamar pada lowongan yang tersedia
- 1 Matakuliah dapat menerima banyak mahasiswa dan mahasiswa hanya dapat menjadi asisten di satu matakuliah (1 to N)
- Fungsionalitas penghubung mahasiswa dan matkul belum selesai
- Log merepresentasikan pekerjaan yang dilakukan oleh asisten untuk akhirnya dilaporkan kepada fakultas
- API mampu mengembalikan sebuah laporan yang berisikan semua pekerjaan asisten pada bulan tersebut dan sebuah summary jumlah uang yang mereka dapatkan.
- 350 Greil / jam 
- Hubungan antar table belom dituliskan
- Sebuah log dapat saja berisikan data lebih singkat dari satu jam
- Konfigurasi database tersebut bisa anda utak-atik sesuai konfigurasi lokal
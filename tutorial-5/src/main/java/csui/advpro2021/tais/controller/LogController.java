package csui.advpro2021.tais.controller;


import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {

    @Autowired
    private LogService logService;

    // Tambah Log
    @PostMapping(path = "/TambahLog/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.tambahLog(npm, log));
    }

    // Gel all logs
    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    // Gel Log by NPM
    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getLogByNpm(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getLogByMahasiswa(npm));
    }
    //  Report teachingAssistant
    @GetMapping(path = "/laporan/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getReportFirst(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getReportFirst(npm));
    }

    // Update log
    @PutMapping(path = "/{idLog}/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") int idLog, @RequestBody Map<String, String> json) {
        return ResponseEntity.ok(logService.updateLog(idLog, json.get("startDate"), json.get("endDate"), json.get("deskripsi")));
    }

    // Delete log
    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteMahasiswa(@PathVariable(value = "idLog") int idLog) {
        logService.deleteLogById(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }



}

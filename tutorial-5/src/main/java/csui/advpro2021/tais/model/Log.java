package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "log")
@Data
@Getter
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idLog;

    @Column
    @JsonFormat(pattern = "Bulan HH:mm")
    private String startDate;

    @Column
    @JsonFormat(pattern = "Bulan HH:mm")
    private String endDate;

    @Column(name = "Deskripsi")
    private String deskripsi;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "mahasiswa_npm", nullable = true)
    @JsonIgnore
    private Mahasiswa mahasiswa;

    public Log(int idLog, String startDate, String endDate, String deskripsi) {
        this.idLog = idLog;
        this.startDate = startDate;
        this.endDate = endDate;
        this.deskripsi = deskripsi;
    }
}


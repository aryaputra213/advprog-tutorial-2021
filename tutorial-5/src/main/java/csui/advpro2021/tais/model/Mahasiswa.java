package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "mahasiswa")
@Data
@Getter
@Setter
@NoArgsConstructor
public class Mahasiswa {

    @Id
    @Column(name = "npm", updatable = false, nullable = false)
    private String npm;

    @Column(name = "nama")
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "ipk")
    private String ipk;

    @Column(name = "no_telp")
    private String noTelp;

    @Column(name = "status_asdos")
    private Boolean status;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "matakuliah_kodeMatkul", nullable = true)
    @JsonIgnore
    private MataKuliah mataKuliah;
    
    @OneToMany(mappedBy = "mahasiswa", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Log> logs;

    public Mahasiswa(String npm, String nama, String email, String ipk, String noTelp, Boolean status) {
        this.npm = npm;
        this.nama = nama;
        this.email = email;
        this.ipk = ipk;
        this.noTelp = noTelp;
        this.status = status;
        this.logs = new ArrayList<>();
    }
}

package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mata_kuliah")
@Data
@NoArgsConstructor
public class MataKuliah {

    @Id
    @Column(name = "kode_matkul", updatable = false)
    private String kodeMatkul;

    @Column(name = "nama_matkul")
    private String nama;

    @Column(name = "prodi")
    private String prodi;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy = "mataKuliah", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Mahasiswa> mahasiswas;

    public MataKuliah(String kodeMatkul, String nama, String prodi, Boolean status) {
        this.kodeMatkul = kodeMatkul;
        this.nama = nama;
        this.prodi = prodi;
        this.status = status;
        this.mahasiswas = new ArrayList<>();
    }
}

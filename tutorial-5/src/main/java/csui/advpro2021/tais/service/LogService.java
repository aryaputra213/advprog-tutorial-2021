package csui.advpro2021.tais.service;

import java.util.Map;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LogService {
    Log createLog(Log Log);

    Iterable<Log> getListLog();

    Iterable<Log> getLogByMahasiswa(String npm);

    Log updateLog(int idLog, String startDate, String endDate, String deskripsi);

    Log tambahLog(String npm,  Log log);

    void deleteLogById(int IdLog);

    Iterable<Map<String, Object>> getReportFirst(String npm);
}

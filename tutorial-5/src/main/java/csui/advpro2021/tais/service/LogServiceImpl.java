package csui.advpro2021.tais.service;

import java.sql.Date;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    final static double GAJI = (double)350/60;

    @Override
    public Log createLog(Log log) {
        logRepository.save(log);
        return log;
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Iterable<Log> getLogByMahasiswa(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa == null) {
            return null;
        }
        return logRepository.findByMahasiswa(mahasiswa);
    }

    @Override
    public Log updateLog(int idLog, String startDate, String endDate, String deskripsi) {
        Log log = logRepository.findById(idLog).orElse(null);
        return updateLog(log, startDate, endDate, deskripsi);

    }


    private Log updateLog(Log log, String startDate, String endDate, String deskripsi) {
        if (log == null) return null;
        else {
            log.setStartDate(startDate);
            log.setEndDate(endDate);
            log.setDeskripsi(deskripsi);
            logRepository.save(log);
            return log;
        }
    }

    @Override
    public void deleteLogById(int IdLog) {
        logRepository.deleteById(IdLog);
    }

    @Override
    public Log tambahLog(String npm, Log log) {
        if(mahasiswaRepository.findByNpm(npm).getStatus()) {
            log.setMahasiswa(mahasiswaRepository.findByNpm(npm));
            return logRepository.save(log);
        }return null;
    }

     @Override
     public Iterable<Map<String, Object>> getReportFirst(String npm) {
         if (!mahasiswaRepository.findByNpm(npm).getStatus()) {
             return null;
            }
            Iterable<Log> logs = getLogByMahasiswa(npm);
            if (logs == null) {
                return null;
         }return getReport2(logs);
        }
        
        private Iterable<Map<String, Object>> getReport2(Iterable<Log> logList) {
                String[] monthList = {"januari", "februari", "maret", "april", "mei", "juni", "juli", "agustus", "september", "oktober", "november", "desember"};
                HashMap<String,Integer> noBulan = new HashMap<>();
                ArrayList<Map<String, Object>> report = new ArrayList<Map<String, Object>>();
                for (int i = 0; i < monthList.length; i++) {
                    HashMap<String, Object> tmpMap = new HashMap<String, Object>();
                    tmpMap.put("jamKerja", 0.0);
                    tmpMap.put("Bulan", monthList[i]);
                    tmpMap.put("Pembayaran", 0.0);
                    noBulan.put(monthList[i], i);
                    report.add(tmpMap);
                }
                for (Log log : logList) {
                    String[] starDate = log.getStartDate().split(" ");
                    String[] endDate = log.getEndDate().split(" ");
                    LocalTime start = LocalTime.parse(starDate[1]);
                    LocalTime end = LocalTime.parse(endDate[1]);
                    double durasi = (double) ChronoUnit.MINUTES.between(start, end);
                    double pembayaran = (double) durasi * GAJI;
                    double durasiJam = (double)durasi/60;
                    report.get(noBulan.get(starDate[0].toLowerCase())).put("jamKerja", (double) report.get(noBulan.get(starDate[0].toLowerCase())).get("jamKerja") + durasiJam);
                    report.get(noBulan.get(starDate[0].toLowerCase())).put("Pembayaran", Math.ceil((double) report.get(noBulan.get(starDate[0].toLowerCase())).get("Pembayaran") + pembayaran));
                }
                return report;
            }
    }

package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.print.attribute.standard.Media;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean 
    private MahasiswaServiceImpl mahasiswaService;

    private Log log;

    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        log = new Log();
        log.setIdLog(0);
        log.setStartDate("Januari 12:00");
        log.setEndDate("Januari 13:00");
        log.setDeskripsi("Dummy Log");
        mahasiswa = new Mahasiswa("123456789", "DummyUser", "Dummy@gmail.com", "4", "081292907790", false);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test 

    public void testControllerPostLog() throws Exception {
        when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
        when(logService.createLog(log)).thenReturn(log);

        mvc.perform(post("/log/TambahLog/123456789").contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(log)));
    }

    @Test
    public void testControllerGetLogbyNpm() throws Exception {
        log.setMahasiswa(mahasiswa);
        Mahasiswa mahasiswa = log.getMahasiswa();
        ArrayList<Log> dummyArray = new ArrayList<Log>();
        dummyArray.add(log);
        when(logService.getLogByMahasiswa(mahasiswa.getNpm())).thenReturn(dummyArray);
        mvc.perform(get("/log/123456789")).andExpect(status().isOk());
    }

    @Test

    public void testControllerGetAllLogs() throws Exception {
        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].startDate").value("Januari 12:00"));
    }

    @Test

    public void testControllerGetExistLog() throws Exception {
        mvc.perform(get("/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test 

    public void testControllerUpdateLog() throws Exception{
        Map<String, String> map = new HashMap<String, String>();
        map.put("startDate", "Januari 12:00");
        map.put("endDate", "Januari 13:00");
        map.put("deskripsi", "test log 2");
        log.setDeskripsi("test log 2");

        when(logService.updateLog(log.getIdLog(), map.get("start"), map.get("end"), map.get("deskripsi"))).thenReturn(log);
    
        mvc.perform(put("/log/0/123456789")
        .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(map)))
        .andExpect(status().isOk());
    }

    @Test 

    public void testControllerDeleteLog() throws Exception {
        logService.createLog(log);
        mvc.perform(delete("/log/0").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerGetReport() throws Exception{
        mvc.perform(get("/log/laporan/1906292931"))
                .andExpect(content().string("[]"));
    }
    
}


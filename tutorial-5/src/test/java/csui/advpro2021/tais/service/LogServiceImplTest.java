package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)

public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    
    @InjectMocks
    private LogServiceImpl logService;

    private Log log;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() throws Exception {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        log = new Log();
        log.setIdLog(1);
        log.setStartDate("Januari 12:00");
        log.setEndDate("Desember 13:00");
        log.setDeskripsi("Dummy");
        log.setMahasiswa(mahasiswa);
        
    }

    @Test 
    public void testServiceCreateLog() throws Exception {
        lenient().when(logService.createLog(log)).thenReturn(log);
    }

    @Test
    public void testServiceGetListLog() throws Exception {
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void testServiceGetLogByNpm() throws Exception {
        Iterable<Log> listLog = logRepository.findByMahasiswa(mahasiswa);
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.getLogByMahasiswa(mahasiswa.getNpm())).thenReturn(listLog);
        Iterable<Log> resultLog = logService.getLogByMahasiswa(mahasiswa.getNpm());
        assertEquals(listLog, resultLog);
    }

    @Test
    public void testserviceDeleteLog() throws Exception {
        logService.createLog(log);
        logService.deleteLogById(0);
        lenient().when(logService.getLogByMahasiswa(mahasiswa.getNpm())).thenReturn(null);
        assertEquals(null, logService.getLogByMahasiswa(mahasiswa.getNpm()));
    }

    @Test
    public void testServiceUpdateLogSuccess() throws Exception {
        Log dummy;
        lenient().when(logRepository.findById(1)).thenReturn(Optional.of(log));
        dummy = logService.updateLog(1, "Dasember 21:00", "Desember 22:00", "Dummy Log");
        assertEquals(log, dummy);
    }
    
    @Test
    public void testServiceUpdateLogNull() throws Exception {
        lenient().when(logRepository.findById(1)).thenReturn(Optional.empty());
        Log dummy = logService.updateLog(1, "Dasember 21:00", "Desember 22:00", "Dummy Log");
        assertEquals(null, dummy);
    }

    @Test

    public void testTambahLog() throws Exception {
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswa.setStatus(true);
        Log dummy = logService.tambahLog(mahasiswa.getNpm(), log);
        Log dummy2 = logService.tambahLog(mahasiswa.getNpm(), log);
        assertEquals(dummy2, dummy);
    }

    @Test

    public void testTambahLogNonAsdos() throws Exception {
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswa.setStatus(false);
        Log dummy = logService.tambahLog(mahasiswa.getNpm(), log);
        assertEquals(null, dummy);
    }

    @Test

    public void testServiceReportSuccess() {
        Iterable<Map<String, Object>> dummy;
        List<Log> listLog;

        listLog = new ArrayList<Log>();
        listLog.add(log);

        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.getLogByMahasiswa(mahasiswa.getNpm())).thenReturn(listLog);
        mahasiswa.setStatus(true);
        dummy = logService.getReportFirst(mahasiswa.getNpm());
        assertNotEquals(null, dummy);
    }

    @Test

    public void testServiceNonAsdos() {
        Iterable<Map<String, Object>> dummy;
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswa.setStatus(false);
        dummy = logService.getReportFirst(mahasiswa.getNpm());
        assertEquals(null, dummy);
    }

    @Test 
    public void testServiceNullLogs() {
        Iterable<Map<String, Object>> dummy;
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.getLogByMahasiswa(mahasiswa.getNpm())).thenReturn(null);
        mahasiswa.setStatus(true);
        dummy = logService.getReportFirst(mahasiswa.getNpm());
        assertEquals(null, dummy);
    }
    

}

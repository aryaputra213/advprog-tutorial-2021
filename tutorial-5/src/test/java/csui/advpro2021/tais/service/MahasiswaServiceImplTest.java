package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MahasiswaServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;
    
    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;
    
    private Mahasiswa mahasiswa;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        mataKuliah = new MataKuliah("DMY", "Dummy", "Dummmy",false);
    }

    @Test
    public void testServiceCreateMahasiswa(){
        lenient().when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
    }

    @Test
    public void testServiceGetListMahasiswa(){
        Iterable<Mahasiswa> listMahasiswa = mahasiswaRepository.findAll();
        lenient().when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        Iterable<Mahasiswa> listMahasiswaResult = mahasiswaService.getListMahasiswa();
        Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);
    }

    @Test
    public void testServiceGetMahasiswaByNpm(){
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }

    @Test
    public void testServiceDeleteAsdos(){
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswaService.deleteMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(null, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test

    public void testServiceDeleteMahasiswaNonAsdos() {
        // Mahasiswa dummy;
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswa.setStatus(false);
        mahasiswaService.deleteMahasiswaByNPM(mahasiswa.getNpm());
        assertNotEquals(null, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test

    public void testServiceDeleteMahasiswaAsdos() {
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswa.setStatus(true);
        mahasiswaService.deleteMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test
    public void testServiceUpdateMahasiswa(){
        Mahasiswa dummy;
        mahasiswaService.createMahasiswa(mahasiswa);
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        dummy = mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);
        assertEquals(mahasiswa, dummy);
    }

    @Test

    public void testServiceUpdateMahasiswaNull() {
        Mahasiswa dummy;
        mahasiswaService.createMahasiswa(mahasiswa);
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswa.setStatus(true);
        dummy = mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);
        assertEquals(null, dummy);
    }


    @Test
    public void testServiceDaftarAsdos() {
        Mahasiswa dummy;
        lenient().when(mataKuliahRepository.findByKodeMatkul(mataKuliah.getKodeMatkul())).thenReturn(mataKuliah);
        dummy = mahasiswaService.daftarAsdos(mataKuliah.getKodeMatkul(), mahasiswa);
        assertEquals(mahasiswa, dummy);
    }

    @Test 
    public void testServiceDaftarAsdosMatkulNotFound() {
        Mahasiswa dummy;
        lenient().when(mataKuliahRepository.findByKodeMatkul(mataKuliah.getKodeMatkul())).thenReturn(null);
        dummy = mahasiswaService.daftarAsdos(mataKuliah.getKodeMatkul(), mahasiswa);
        assertEquals(null, dummy);
        assertEquals(false, mahasiswa.getStatus());
    }
}

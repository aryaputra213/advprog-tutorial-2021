package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {
    int hasil;
    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            hasil = rawAge*2000;
        } else if (rawAge <50) {
            hasil = rawAge*2250;
        } else {
            hasil = rawAge*5000;
        }
        return  hasil;
    }

    @Override
    public  String classFromBirthYear() {
        if (hasil > 0 && hasil < 20000) {
            return "C CLass";
        } else if (hasil > 20000 && hasil < 100000) {
            return  "B Class";
        } else {
            return  "A Class";
        }
    }
    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        System.out.println(currentYear);
        return currentYear-birthYear;
    }
}
